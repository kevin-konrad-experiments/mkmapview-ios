import UIKit
import MapKit
import CoreLocation

class StoresMap: UIViewController {
    @IBOutlet weak var mapView: MKMapView!
    var locationManager: CLLocationManager!
    weak var storeProvider: StoreProvider?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.delegate = self

        if CLLocationManager.locationServicesEnabled() {
            let manager = CLLocationManager()
            manager.requestWhenInUseAuthorization()
            self.locationManager = manager
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.mapView.removeAnnotations(self.mapView.annotations)
        
        if let stores = self.storeProvider?.stores {
            self.mapView.addAnnotations(stores.map {
                $0.annotation
                
            })
        }
    }
}

extension StoresMap: MKMapViewDelegate {
    public static let appleStoreIdentifier = "ASI"
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation {
            return nil
        }
        
        if let reused = mapView.dequeueReusableAnnotationView(withIdentifier: StoresMap.appleStoreIdentifier) {
            reused.annotation = annotation
            return reused
        }
        
        let pin = MKPinAnnotationView(annotation: annotation, reuseIdentifier: StoresMap.appleStoreIdentifier)
        pin.canShowCallout = true
        pin.pinTintColor = .blue
        return pin
    }
}
