import Foundation
import UIKit

public class AppBase : UIViewController {
    public override func viewDidLoad() {
        super.viewDidLoad()
        self.edgesForExtendedLayout = .init(rawValue: 0)
    }
}
